import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'

Vue.use(VueRouter)

import Home from './components/Home'
import Login from './components/user/Login'
import Register from './components/user/Register'
import Profile from './components/user/Profile'
import Logout from './components/user/Logout'
import New from './components/product/New'
import Product from './components/product/Product'

const routes = [
	{ path: '/', component: Home, meta: { header: true } },
	{ path: '/new', component: New, meta: { header: true } },
	{ path: '/product/:id', component: Product, meta: { header: true } },
	{ path: '/login', component: Login, meta: { header: true } },
	{ path: '/register', component: Register, meta: { header: true } },
	{ path: '/profile', component: Profile, meta: { header: true } },
	{ path: '/logout', component: Logout, meta: { header: false } },
]

const router = new VueRouter({
	mode: 'history',
  routes
})

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})